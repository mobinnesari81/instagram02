package sbu.cs.Classes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CommentObject {
    // Variables:
    private int ID;
    private int userID;
    private int postID;
    private String commentText;
    private String commentDate;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");



    // Functions:
        // Constructors:
    public CommentObject(int ID, int userID, int postID, String commentText, String commentDate) //Normal constructor
    {
        this.ID = ID;
        this.userID = userID;
        this.postID = postID;
        this.commentText = commentText;
        this.commentDate = commentDate;
    }

    public CommentObject(int userID, int postID, String commentText) // New comment constructor
    {
        this.ID = 0;
        this.userID = userID;
        this.postID = postID;
        this.commentText = commentText;
        this.commentDate = LocalDateTime.now().format(dateFormat);
    }

        // Getters:


    public int getID() {
        return ID;
    }

    public int getUserID() {
        return userID;
    }

    public int getPostID() {
        return postID;
    }

    public String getCommentText() {
        return commentText;
    }

    public String getCommentDate() {
        return commentDate;
    }

        // Setters:


    public void setID(int ID) {
        this.ID = ID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }
}
