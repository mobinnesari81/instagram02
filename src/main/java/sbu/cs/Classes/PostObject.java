package sbu.cs.Classes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class PostObject {
    // Variables:
    private int ID;
    private int userID;
    private String pictureAddress;
    private String caption;
    private String postDate;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");


    // Functions:
        // Constructors:
    public PostObject(int id, int userID, String pictureAddress, String caption, String postDate) //Normal constructor
    {
        this.ID = id;
        this.userID = userID;
        this.pictureAddress = pictureAddress;
        this.caption = caption;
        this.postDate = postDate;
    }

    public PostObject(int userID, String pictureAddress, String caption) // Create a new post
    {
        this.ID = 0;
        this.userID = userID;
        this.pictureAddress = pictureAddress;
        this.caption = caption;
        this.postDate = LocalDateTime.now().format(dateFormat);
    }

        // Getters:

    public int getID() {
        return ID;
    }

    public int getUserID() {
        return userID;
    }

    public String getPictureAddress() {
        return pictureAddress;
    }

    public String getCaption() {
        return caption;
    }

    public String getPostDate() {
        return postDate;
    }

        // Setters:

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setPictureAddress(String pictureAddress) {
        this.pictureAddress = pictureAddress;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
