package sbu.cs.Classes;

public class LikeObject {
    //Variables:
    private int postID;
    private int userID;

    //Functions:
        //Constructor:
    public LikeObject(int postID, int userID)
    {
        this.postID = postID;
        this.userID = userID;
    }

        //Getter:

    public int getPostID() {
        return postID;
    }

    public int getUserID() {
        return userID;
    }

        //Setters:


    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
