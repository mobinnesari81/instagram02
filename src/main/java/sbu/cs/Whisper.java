package sbu.cs;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class Whisper {
    // Variables:
    private String request;
    private Map<String, String> data;

    // Functions:
    public Whisper(String request, Map<String, String> data)
    {
        this.request = request;
        this.data = data;
    }

    public String getRequest()
    {
        return this.request;
    }

    public Map<String, String> getData()
    {
        return this.data;
    }

    public void setRequest(String request)
    {
        this.request = request;
    }

    public void setData(Map<String, String> data)
    {
        this.data = data;
    }

    public Set<String> getKeys()
    {
         return this.data.keySet();
    }

    public Collection<String> getValues() {
        return this.data.values();
    }

    public String getValueWithKey(String key)
    {
        return this.data.get(key);
    }

}
